function profile(uid) {
    // for / websites
    window.open("/users/" + uid);
}

function post(tid) {
    // for / websites
    window.open("/posts/" + tid);
}

function phpProfile(puid) {
    // for users.php?id=
    window.open("users.php?uid=" + puid);
}

function phpPost(ptid) {
    // for posts.php?
    window.open("posts.php?tid=" + ptid);
}

// no Question marks in submit

function nqProfile(nquid) {
    window.open("user." + nquid + ".php");
}

function nqPost(nqtid) {
    window.open("post." + nqtid + ".php");
}