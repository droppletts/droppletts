<?

namespace droppletts;

class settings {
    $theme = 2;
    $basefont = false;
    $blocked = false;

    if ($theme == 1) {
        echo "<META NAME='color-scheme' CONTENT='dark'/>";
    } else if ($theme == 2) {
        echo "<META NAME='color-scheme' CONTENT='light'/>";
    } else if ($theme == 3) {
        echo "<META NAME='color-scheme' CONTENT='light dark'/>";
    }

    if ($basefont == true) {
        echo "<STYLE>body, html {font-family:Arial, sans-serif, monospace;}</STYLE>";
    } else if ($basefont == false) {
        // null
    } else {
        return null;
    }

    if ($blocked == true) {
        echo "<META HTTP-EQUIV='refresh' CONTENT='0;index.php'/>";
    } else if ($blocked == false) {
        // null
    } else {
        return null;
    }
}